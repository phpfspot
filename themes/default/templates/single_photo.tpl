<!-- Single photo -->
<div class="header">
 <div class="header_title">
  <b>Photo {$image_name}</b><br />
  {if $photo_count > 0}
   {$photo_number} of {$photo_count}
   {if $photo_count == 1}
    Photo
   {else}
    Photos
   {/if}
   {if isset($description) && $description != "" }
    -&nbsp;{$description}<br />
   {/if}
  {/if}
 </div>
 <div class="header_menu">
  <a href="javascript:zoom(-10);" title="zoom_out" onclick="click(this);"><img src="{$web_path}/resources/zoom-out.png" /></a>
  <a href="javascript:zoom(0);" title="zoom_100" onclick="click(this);"><img src="{$web_path}/resources/zoom-100.png" /></a>
  <a href="javascript:zoom(10);" title="zoom_in" onclick="click(this);"><img src="{$web_path}/resources/zoom-in.png" /></a>
  <a href="javascript:startAutoBrowse();" title="auto browsing"><img id="autobrowse_ico" src="{$web_path}/resources/16_play.png" />&nbsp;Auto-Browse</a>
  {if isset($extern_link) }
   <a href="{$extern_link}" title="Use this link to return to the current view"><img src="{$web_path}/resources/link.png" />&nbsp;External Link</a>
  {/if}
  <a href="{$image_url_full}" title="orig_image" target="_blank" onclick="click(this);"><img src="{$web_path}/resources/original.png" alt="original resolution" />&nbsp;Fullsize</a>
 </div>
</div>

<p class="cb">

 <div id="photo_details">
   { if isset($ExifMadeWith) }
   <u><img src="{$web_path}/resources/camera.png" alt="camera icon" />&nbsp;Image taken with:</u><br />
   {$ExifMadeWith}<br />
   { /if }
   { if isset($ExifMadeOn) }
   <u><img src="{$web_path}/resources/date.png" alt="calendar icon" />&nbsp;Image made on:</u><br />
   {$ExifMadeOn}<br />
   { /if }
   { if isset($ExifOrigResolution) }
   <u><img src="{$web_path}/resources/image.png" alt="resolution icon" />&nbsp;Original resolution:</u><br />
   {$ExifOrigResolution}px<br />
   { /if }
   <u>Size:</u>&nbsp;{$ExifFileSize}<br />
   { if isset($image_rating) && $image_rating > 0 }
    <u><img src="{$web_path}/resources/rating.png" alt="rating" />&nbsp;Rating:</u><br />
    {section name="rating" loop=$image_rating step=1}
     <img src="{$web_path}/resources/star.png" />
    {/section}
    <br />
   { /if }
   { if isset($has_versions) }
    <br />
    Versions:<br />
    <select onchange="update_photo_version(this, {$photo});">
    { photo_version_select_list photo=$photo current=$version }
    </select>
    <br />
   { /if }
   { if isset($tags) }
    <br />
    <u><img src="{$web_path}/resources/small_available_tags.png" alt="available tags" />&nbsp;Tagged with:</u><br />
    { foreach from=$tags key=id item=name }
    <a class="smalltag" href="javascript:Tags('add', '{$id}');" onfocus="click(this);">{$name}</a><br />
    { /foreach }
   { /if }
   <hr>
   { if isset($prev_img) && isset($previous_url) }
    <u><img src="{$web_path}/resources/photo.png" alt="photo icon" />&nbsp;Previous:</u><br />
    <a href="{$previous_url}" onfocus="click(this);" id="prev_link">
     <img src="{$web_path}/phpfspot_img.php?idx={$prev_img}&amp;width={$mini_width}" /><br />
    </a>
   { /if}
   { if isset($next_img) && isset($next_url) }
    <u><img src="{$web_path}/resources/photo.png" alt="photo icon" />&nbsp;Next:</u><br />
    <a href="{$next_url}" onfocus="click(this);" id="next_link">
     <img src="{$web_path}/phpfspot_img.php?idx={$next_img}&amp;width={$mini_width}" /><br />
    </a>
   { /if}
 </div>

 <!-- the photo itself -->

 <div class="photo">
 { if !isset($next_url) || $next_url == "" }
  <a href="{$image_url}" onclick="showPhotoIndex(); return false;" onfocus="click(this);" name="photo">
 { else }
  <a href="{$image_url}" onclick="{$next_url}; return false;" onfocus="click(this);" title="click for the next photo" name="photo">
 { /if }
   <img src="{$image_url}" width="{$width}" height="{$height}" alt="{$image_filename}" name="photo" id="photo" />
  </a>
 </div>

 <!-- navigation arrows -->

 <div class="photonavigation">

  <!-- previous -->
  { if !isset($previous_url) || $previous_url == "" }
   <img src="{$web_path}/resources/arrow_left_gray.png" alt="first photo reached" />
  { else }
   <a href="{$previous_url}" onfocus="click(this);" title="click for the previous photo (left cursor)">
    <img src="{$web_path}/resources/arrow_left.png" alt="previous photo" />
   </a>
  { /if }

  <!-- back to index -->
   <a href="javascript:showPhotoIndex({$current_page}, {$current_img});" onfocus="click(this);" title="click to go back to the index">
    <img src="{$web_path}/resources/arrow_up.png" alt="photo index" />
   </a>

  <!-- next -->
  { if !isset($next_url) || $next_url == "" }
   <img src="{$web_path}/resources/arrow_right_gray.png" alt="last photo reached" />
  { else }
   <a href="{$next_url}" onfocus="click(this);" title="click for the next photo (right cursor)">
    <img src="{$web_path}/resources/arrow_right.png" alt="next photo" />
   </a>
  { /if}
 </div>
</p>
<script type="text/javascript">
<!--
   if(autobrowse && document.getElementById("autobrowse_ico"))
      document.getElementById("autobrowse_ico").src = '{$web_path}/resources/16_pause.png';

   /* lets preload to previous and the next image to speedup */
   var image_next = new Image();
   image_next.src = "{$web_path}/phpfspot_img.php?idx={$next_img}&width={$photo_width}";
   var image_prev = new Image();
   image_prev.src = "{$web_path}/phpfspot_img.php?idx={$prev_img}&width={$photo_width}";

-->
</script>
<!-- /Single photo -->
