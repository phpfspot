<!-- welcome -->
<div class="header">
 phpfspot welcomes you to a dynamic photo gallery for <a href="http://f-spot.org" target="_blank">F-Spot</a><br />
</div>
<div class="welcome">

 <p>
 <span>
   <img src="{$web_path}/phpfspot_img.php?idx=rand&amp;width=150&amp;i=1" alt="random image" />
 </span>
  This application targets to provide an easy way, to presentate your F-Spot<br />
  pictures in the web, in a way where you do not need any of the current<br />
  web2.0 providers or setup complex apps like gallery
 </p>

 <br class="cb" />

 <p>
 <span>
   <img src="{$web_path}/phpfspot_img.php?idx=rand&amp;width=150&amp;i=2" alt="random image" />
 </span>
  You can adapt this welcome page in the file "welcome.tpl" in the template<br />
  directory of your current theme.
 </p>

 <br class="cb" />

 <p>
 <span>
   <img src="{$web_path}/phpfspot_img.php?idx=rand&amp;width=150&amp;i=3" alt="random image" />
 </span>
 <a href="javascript:showPhotoIndex();">[ Click here to take a look at your photo index or select a tag from the list on the left ]</a>
 </p>
</div>
<!-- /welcome -->
