{include file="header.tpl"}
 <body onload="init_phpfspot('{$web_path}');">
 
  <!-- loading -->

  <div id="HTML_AJAX_LOADING">
   <img src="{$web_path}/resources/loading.gif" style="vertical-align: middle;" alt="Loading..." />&nbsp;&nbsp;Loading...
  </div>

  <!-- top menu -->

  <div class="menu">
   <div class="icons">
    <a href="javascript:showPhotoIndex();" onclick="click(this);" title="Show Photo Index (CTRL+ALT+i)">
     <img src="{$web_path}/resources/photo_index.png" alt="photo index" />
    </a>
    <a href="javascript:resetAll();" onclick="click(this);" title="Reset selected-tags and search results (CTRL+ALT+r)">
     <img src="{$web_path}/resources/reload.png" alt="reset tags" />
    </a>
    <a href="javascript:showCredits();" onclick="click(this);" title="Show's a little credit page">
     <img src="{$web_path}/resources/credits.png" alt="show credits" />
    </a>
   </div>
   <div class="logo">&nbsp;{$page_title}</div>
  </div>
  <!-- /top menu -->

  <div style="clear: both;">
  <!-- option column -->
  <div class="options" style="float: left;">
   <div>
    {include file="search.tpl"}
   </div>
   <div id="tags">
    {include file="tags.tpl"}
   </div>
  </div>
  <!-- /option column -->

  <!-- content column -->
  <div id="content" class="content">
   { $initial_content }
  </div>
  </div>

{include file="footer.tpl"}
