<table class="calendar">
 <tr>
  <td><a href="{$prev_month}" class="prevMonth">&lt;&lt;</a></td>
  <td colspan="5">
   {$current_month}
  </td>
  <td><a href="{$next_month}" class="nextMonth">&gt;&gt;</a></td>
 </tr>
 <tr>
  <td>M</td>
  <td>T</td>
  <td>W</td>
  <td>T</td>
  <td>F</td>
  <td>S</td>
  <td>S</td>
 </tr>
 <!-- matrix -->
 {section name="row" loop=$rows step=1}
  {section name="col" loop=8 step=1}
   {if isset($matrix[row][col]) }
    {$matrix[row][col]}
   {/if}
  {/section}
 {/section}
 <!-- /matrix -->
</table>

