{include file="header.tpl"}
 <script type="text/javascript" src="{$web_path}/slider/js/range.js"></script>
 <script type="text/javascript" src="{$web_path}/slider/js/timer.js"></script>
 <script type="text/javascript" src="{$web_path}/slider/js/slider.js"></script>
 <link type="text/css" rel="StyleSheet" href="{$web_path}/slider/css/bluecurve/bluecurve.css" />
 <body onload="startSlideShow('{$web_path}');" class="slideshow">
  <div id="slide_navigation">
   <a href="javascript:prevSlide();" onclick="click(this);" title="slive to previous photo"><img id="rew_ico" src="{$web_path}/resources/32_rew.png" /></a>
   <a href="javascript:pauseSlideShow();" onclick="click(this);"><img id="pause_ico" src="{$web_path}/resources/32_pause.png" /></a>
   <a href="javascript:startSlideShow();" onclick="click(this);" title="stop and revert slideshow"><img id="stop_ico" src="{$web_path}/resources/32_stop.png" /></a>
   <a href="javascript:nextSlide();" onclick="click(this);" title="slide to next photo"><img id="fwd_ico" src="{$web_path}/resources/32_fwd.png" /></a>
  </div>
  <div id="slide_close">
   <a href="javascript:window.close();" title="click to close slideshow">
    <img id="slide_img" alt="slideshow_img" />
   </a>
  </div>
  <form>
   <div id="slider-1" tabIndex="1" onclick="if(this.blur)this.blur();">
   <input id="slider-input-1" name="slider-input-1" onclick="if(this.blur)this.blur();" />
   </div>
  </form>
  <div id="current_slide_time"></div>
  <script type="text/javascript">
   initSlider();
  </script>
 </body>
{include file="footer.tpl"}
