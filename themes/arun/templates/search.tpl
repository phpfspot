<span class="tags">
Search:
</span>
<form action="" onsubmit="startSearch(); return false;">
<div class="searchfor">
<table>
 <tr>
  <td>Tag:</td>
  <td>
   { if isset($use_autocomplete) }
   <input type="text" name="searchfor_tag" id="searchfor_tag" value="{$searchfor_tag}" size="15" acdropdown="true" autocomplete_list="url:rpc.php?action=getxmltaglist&search=[S]&length=10" />
   { literal }
   <script type="text/javascript">
      var options = {
         script: "rpc.php?action=getxmltaglist&",
         varname: "search",
         json: false,
         maxresults: 15
      };
      var as = new bsn.AutoSuggest('searchfor_tag', options);
   </script>
   { /literal }
   { else }
   <input type="text" name="searchfor_tag" value="{ if isset($searchfor_tag) }{$searchfor_tag}{ /if }" size="15" />
   { /if }
  </td>
  <td>
   <input type="image" class="submit" src="{$web_path}/resources/doit.png" alt="start search" title="start search" onclick="click(this);" />
  </td>
 </tr>
 <tr>
  <td>Date:</td>
  <td class="nowarp">
   { if ! isset($date_search_enabled) }
    <input type="checkbox" class="checkbox" name="consider_date" value="Y" onclick="datesearch();" />
   { else }
    <input type="checkbox" class="checkbox" name="consider_date" value="Y" onclick="datesearch();" checked="checked" />
   { /if }
   consider date-range
  </td>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td>{$search_from_date} <a href="javascript:showCalendar('from');" onclick="click(this);"><img src="{$web_path}/resources/date.png" id="frompic" alt="from" /></a></td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td>{$search_to_date} <a href="javascript:showCalendar('to');" onclick="click(this);"><img src="{$web_path}/resources/date.png" id="topic" alt="to" /></a></td>
  <td>
   <input type="image" class="submit" src="{$web_path}/resources/doit.png" alt="start search" title="start search" onclick="click(this);" />
  </td>
 </tr>
 { if isset($has_rating) }
 { /if }
 <tr>
  <td>
   Sort-Order:&nbsp;
  </td>
  <td>
   <select name="sort_order" onchange="update_sort_order(this);">
    { sort_select_list }
   </select>
  </td>
  <td>
   &nbsp;
  </td>
 </tr>
</table>
</div>
</form>
<div id='calendar'></div>
