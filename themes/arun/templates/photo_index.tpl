<!-- Photo Index -->

<!-- Header - displays count of matched photos and some link's -->

<div class="header">
 <div class="header_title">
  <b>Photo Index</b>
  {if isset($searchfor) }
   {if $count == 1}
    {$count} image is the result for your search about "{$searchfor}".
   {else}
    {$count} images are the result for your search about "{$searchfor}".
   {/if}
  {elseif isset($tag_result)}
   {if $count == 1}
    {$count} image has been found for the selected tags.
   {else}
    {$count} images have been found for the selected tags.
   {/if}
  {else}
   {if $count == 1}
    {$count} image has been found.
   {else}
    {$count} images have been found.
   {/if}
  {/if}
  {if isset($from_date) && isset($to_date) }
   <br />
   Results are limited to a date within {$from_date} to {$to_date}.
  {/if}
 </div>
 <div class="header_menu">
  {if isset($rss_link) }
   <a href="{$rss_link}" target="_blank" title="RSS feed"><img src="{$web_path}/resources/rss.png" /></a>
  {/if}
 </div>
</div>

<!-- if result of a tag-search is displayed, show the selected tags
     with some small pictures in a bar. -->

{ if isset($tag_result) }
<!-- seperator -->
 <div class="tagresult"></div>
 <!-- /seperator -->
 <div class="tagresulttags" style="float: left;">
  Tags:
 </div>
 <div class="tagresulttags">
  { $selected_tags }
 </div>
{ /if }

<!-- the photo matrix itself -->

<div id="index">

 {section name="thumb" loop=$thumbs step=1}

  {if isset($images[thumb]) }

   <div class="thumb" style="width: { $thumb_container_width }px; height: { $thumb_container_height }px;">
    { if isset($user_friendly_url) }
    <a href="{$web_path}/photoview/{$images[thumb]}" onclick="showPhoto({$images[thumb]}, 'scrollup'); return false;" id="thumblink{$images[thumb]}" name="image{$img_id[thumb]}" class="thumblink" onfocus="click(this);" title="{$img_title[thumb]}">
    { else }
    <a href="{$web_path}/index.php?mode=showp&amp;id={$images[thumb]}" onclick="showPhoto({$images[thumb]}, 'scrollup'); return false;" id="thumblink{$images[thumb]}" name="image{$img_id[thumb]}" class="thumblink" onfocus="click(this);" title="{$img_title[thumb]}">
    { /if }
     <img class="thumb" id="thumbimg{$images[thumb]}" width="{$img_width[thumb]}" height="{$img_height[thumb]}" src="{$web_path}/phpfspot_img.php?idx={$images[thumb]}&width={$width}" />
     <br />
    {$img_name[thumb]}
    </a>
    <!-- show lightbox eyes, if enabled -->
    { if isset($use_lightbox) }
    <a href="{$web_path}/phpfspot_img.php?idx={$images[thumb]}&amp;width={$preview_width}" alt="thumb_{$images[thumb]}" rel="lightbox[photoidx]" title="Preview of {$img_fullname[thumb]}"><img src="{$web_path}/resources/eyes.png" /></a>
    { /if }
    <!-- show F-Spot photo rating value, if available -->
    { if isset($img_rating[thumb]) }
     <br />
     {section name="rating" loop=$img_rating[thumb] step=1}
      <img src="{$web_path}/resources/star.png" />
     {/section}
    { /if }
   </div>

  {/if}

 {/section}

</div>
<br class="cb" />
<div class="indexnavigation">

 <!-- the right nav arrow -->
 <div class="indexnavigationright">
 { if !isset($next_url) || $next_url == "" }
  { if $count != 0 }
   <img src="{$web_path}/resources/arrow_right_gray.png" alt="last page reached" />
  { /if }
 { else }
  <a href="{$next_url}" id="next_link" title="click for the next page (right cursor)">
   <img src="{$web_path}/resources/arrow_right.png" alt="next photo" />
  </a>
 { /if}
 </div>

 <!-- the left nav arrow -->
 <div class="indexnavigationleft">
 { if !isset($previous_url) || $previous_url == "" }
  { if $count != 0 }
   <img src="{$web_path}/resources/arrow_left_gray.png" alt="first page reached" />
  { /if }
 { else }
  <a href="{$previous_url}" id="prev_link" title="click for the previous page (left cursor)">
  <img src="{$web_path}/resources/arrow_left.png" alt="previous photo" />
  </a>
 { /if }
 </div>

 <!-- the middle page selector -->
 <div class="indexnavigationcenter">
 { if isset($page_selector) && $page_selector != "" }
  {$page_selector}
 { /if}
 </div>
</div>
<br class="cb" />
<script type="text/javascript" language="JavaScript">
<!--
   // image preloading
   //
   // these few javascript lines will try to speed up loading all the photo
   // thumbnails. An array of all photos to display will be built written
   // by the smarty-template. The js-code will then loop through this array
   // and invoke loading of each photo. Sadly this does not work good on all
   // browsers.
   { counter start=-1 }

   var current;
   var image_urls = new Array();
   var last_thumb;

   {section name="thumb" loop=$thumbs step=1}
      {if isset($images[thumb]) }
         image_urls[{counter}] = '{$web_path}/phpfspot_img.php?idx={$images[thumb]}&width={$width}';
         last_thumb = {$images[thumb]};
      {/if}
   {/section}

   preloadPhotos(image_urls);

   // auto-scroll
   //
   // if browser is to far down the page, that he can't see the photo at all
   // scroll it up so that at least the last photo becomes visisble.
   { literal }

   var ywnd = 0;
   var yimg = 0;
   // check where we are with the browser
   if (window.pageYOffset) {
      ywnd = window.pageYOffset;
   } else if (document.body && document.body.scrollTop) {
      ywnd = document.body.scrollTop;
   }
   // check the y-pos of the last thumbnail
   if(thumbimg = document.getElementById('thumbimg' + last_thumb)) {
      yimg = findPos(thumbimg,'top');
   }
   // if the browser-window is scrolled further then the last_thumb, scroll back
   if(ywnd > yimg) {
      window.scrollTo(0, yimg-100);
   }

   { /literal }
   // auto-scroll

-->
</script>
<!-- /Photo Index -->
