<div class="header">
 <b>{$product} {$version}</b><br />
</div>
<div class="credits">
 by Andreas Unterkircher<br />
 Licensed under GPLv3, <a href="http://oss.netshadow.at/oss/phpfspot/Project" target="_blank">phpfspot</a>
 <hr />
 Some extensions:<br /><br />
 <a href="http://wiki.bluga.net/HTML_AJAX/HomePage">HTML_AJAX</a> - AJAX communication with the server
 <br />
 <a href="http://prism-perfect.net/archive/php-tag-cloud-tutorial/">Jenny Ferenc</a> - PHP tag-cloud code
 <br />
 <a href="http://www.silvestre.com.ar/">Silvestre Herrera's</a> - the most icons are from the Nuovo icon kit.
 <br />
 <a href="http://www.huddletogether.com/projects/lightbox2/">Lightbox2</a> - photo preview
 <br />
 <a href="http://www.brandspankingnew.net/archive/2007/02/ajax_auto_suggest_v2.html">Ajax Auto Suggest</a> - Auto-Completion
 <hr />
 F-Spot database version: {$db_version}
</div>
