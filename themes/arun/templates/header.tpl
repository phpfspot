<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>{$page_title}</title>
  <link href="{$web_path}/{$template_path}/stylesheet.css" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="{$web_path}/rpc.php?mode=init&amp;client=all"></script>
  <script type="text/javascript" src="{$web_path}/phpfspot.js"></script>
  <link rel="shortcut icon" href="{$web_path}/resources/gpl_16.png" type="image/png" />
  <link rel="icon" href="{$web_path}/resources/gpl_16.png" type="image/png" />
  { if isset($use_lightbox) }
  <script type="text/javascript" src="{$web_path}/lightbox2/js/prototype.js"></script>
  <script type="text/javascript" src="{$web_path}/lightbox2/js/scriptaculous.js?load=effects,builder"></script>
  <script type="text/javascript" src="{$web_path}/lightbox2/js/lightbox.js"></script>
  <link rel="stylesheet" href="{$web_path}/lightbox2/css/lightbox.css" type="text/css" media="screen" />
  { /if }
  { if isset($use_autocomplete) }
  <script type="text/javascript" src="{$web_path}/autosuggest/js/bsn.AutoSuggest_2.1.3_comp.js"></script>
  <link rel="stylesheet" href="{$web_path}/autosuggest/css/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf-8" />
  { /if }
 </head>
