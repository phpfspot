<?php

/* *************************************************************************
 *
 * phpfspot, presents your F-Spot photo collection in Web browsers.
 *
 * Copyright (c) by Andreas Unterkircher
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * *************************************************************************/

require_once "phpfspot.class.php";

/**
 * PHPFSPOT_RPC class
 *
 * handles AJAX-RPC calls from client browsers
 * @package phpfspot
 */
class PHPFSPOT_RPC {

   /**
    * PHPFSPOT_RPC constructor
    */
   public function __construct()
   {
      /* start PHP session */
      session_start();

   } // __construct()

   public function process_ajax_request()
   {
      require_once 'HTML/AJAX/Server.php';

      $server = new HTML_AJAX_Server();
      $server->handleRequest();

      $phpfspot = new PHPFSPOT();

      /* if no action is specified, no need to further process this
       * function here.
       */
      if(!isset($_GET['action']) && !isset($_POST['action']))
         return;

      if(isset($_GET['action']))
         $action = $_GET['action'];
      if(isset($_POST['action']))
         $action = $_POST['action'];

      switch($action) {
         case 'showphoto':
            if(isset($_GET['id']) && is_numeric($_GET['id'])) {
               print $phpfspot->showPhoto($_GET['id']);
            }
            break;
   
         case 'getxmltaglist':
            print $phpfspot->get_xml_tag_list();
            break;

         case 'show_available_tags':
            print $phpfspot->getAvailableTags();
            break;

         case 'show_selected_tags':
            print $phpfspot->getSelectedTags();
            break;

         case 'addtag':
            if(isset($_POST['id']) && is_numeric($_POST['id'])) {
               print $phpfspot->addTag($_POST['id']);
            }
            break;

         case 'deltag':
            if(isset($_POST['id']) && is_numeric($_POST['id'])) {
               print $phpfspot->delTag($_POST['id']);
            }
            break;

         case 'reset':
            $phpfspot->resetTagSearch();
            $phpfspot->resetNameSearch();
            $phpfspot->resetTags();
            $phpfspot->resetDateSearch();
            $phpfspot->resetRateSearch();
            $phpfspot->resetPhotoView();
            break;

         case 'tagcondition':
            if(isset($_POST['mode']) && in_array($_POST['mode'], Array('or', 'and'))) {
               print $phpfspot->setTagCondition($_POST['mode']);
            }
            break;

         case 'show_photo_index':
            if(isset($_GET['begin_with']) && is_numeric($_GET['begin_with'])) {
               $_SESSION['begin_with'] = $_GET['begin_with'];
            }
            else {
               unset($_SESSION['begin_with']);
            }
            if(isset($_GET['last_photo']) && is_numeric($_GET['last_photo']))
               $_SESSION['last_photo'] = $_GET['last_photo'];

            print $phpfspot->showPhotoIndex();
            break;
   
         case 'showcredits':
            $phpfspot->showCredits();
            break;

         case 'search':
            print $phpfspot->startSearch();
            break;

         case 'update_sort_order':
            if(isset($_POST['value']) && is_string($_POST['value'])) {
               print $phpfspot->updateSortOrder($_POST['value']);
            }
            break;

         case 'update_photo_version':
            if(isset($_POST['photo_version']) && is_numeric($_POST['photo_version']) &&
               isset($_POST['photo_idx']) && is_numeric($_POST['photo_idx'])) {
               print $phpfspot->update_photo_version($_POST['photo_idx'], $_POST['photo_version']);
            }
            break;

         case 'get_export':
            /* $_GET['mode'] will be validated by getExport() */
            $phpfspot->getExport($_GET['mode']);
            break;

         case 'get_photo_to_show':
            print $phpfspot->get_current_photo();
            break;

         case 'get_calendar_matrix':
            if(isset($_GET['date']) && is_string($_GET['date'])) {
               $phpfspot->get_calendar_matrix($_GET['date']);
            }
            break;

         case 'what_to_do':
            print $phpfspot->whatToDo();
            break;

         case 'reset_slideshow':
            print $phpfspot->resetSlideShow();
            break;

         case 'get_next_slideshow_img':
            print $phpfspot->getNextSlideShowImage();
            break;
         
         case 'get_prev_slideshow_img':
            print $phpfspot->getPrevSlideShowImage();
            break;

      }

   } // process_ajax_request();

} // class PHPFSPOT_RPC

$rpc = new PHPFSPOT_RPC();
$rpc->process_ajax_request();

?>
