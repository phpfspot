#!/bin/bash

RSYNC="/usr/bin/rsync"

F_SPOT_DB="$HOME/.gnome2/f-spot/photos.db"
F_SPOT_PHOTOS="$HOME/Photos"

DEST="server:/var/www/phpfspot"

rsync -vr --progress --delete -e ssh ${F_SPOT_DB} ${DEST}
rsync -vr --progress --delete -e ssh ${F_SPOT_PHOTOS} ${DEST}
