#!/usr/bin/php
<?php

/***************************************************************************
 *
 * phpfspot, presents your F-Spot photo collection in Web browsers.
 *
 * Copyright (c) by Andreas Unterkircher
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

/**
 * find_dups.php
 *
 * find duplicated photos based on MD5 checksum
 *
 * @package phpfspot
 */
if(!isset($_SERVER["TERM"])) {
   print "<br /><br />This script should only be invoked from command line!<br />\n";
   die;
}

require_once "phpfspot.class.php";

$fspot = new PHPFSPOT;
$fspot->fromcmd = true;

$all_dups = $fspot->cfg_db->db_query("
   SELECT i1.img_idx as i1, i2.img_idx as i2
   FROM images i1
   INNER JOIN images i2
      ON (
         i1.img_md5=i2.img_md5
      AND
         i1.img_idx!=i2.img_idx
      )
   ORDER BY i1.img_idx ASC
");

while($row = $fspot->cfg_db->db_fetch_object($all_dups)) {
   if($photo1 = $fspot->getphotoname($row['i1'])) {
      print "Dups for ". $photo1 ."\n";
      if($photo2 = $fspot->getphotoname($row['i2'])) {
         print "\t". $photo2 ."\n";
      }
   }
}

?>
