<?php

/***************************************************************************
 *
 * phpfspot, presents your F-Spot photo collection in Web browsers.
 *
 * Copyright (c) by Andreas Unterkircher
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

require_once "phpfspot.class.php";

/**
 * PHPFSPOT_IMG class
 *
 * handles phpfspot's photos. It will output either the photo binaries
 * or can also show error messages as a on-the-fly generated picture.
 * @package phpfspot
 */
class PHPFSPOT_IMG {
   
   private $db;
   private $parent;

   /**
    * PHPFSPOT_IMG class constructor
    */
   public function __construct()
   {
      $this->parent = new PHPFSPOT;
      $this->db = $this->parent->db;

   } // __construct()

   /**
    * PHPFSPOT_IMG class destructor
    */
   public function __destruct()
   {

   } // __desctruct()

   /**
    * sends the specified image to the browser
    *
    * this function will send the specified image to 
    * the client - in the specified width. it also try's
    * to create on-the-fly missing thumbnails via PHPFSPOT
    * gen_thumbs function.
    * @param integer $idx
    * @param integer $width
    */
   public function showImg($idx, $width = 0, $version = NULL)
   {
      if($idx == 'rand')
         $idx = $this->parent->get_random_photo();

      /* display the lastest available version, if a wrong version has been requested */
      if(!isset($version) || !$this->parent->is_valid_version($idx, $version))
         $version = $this->parent->get_latest_version($idx);

      $details = $this->parent->get_photo_details($idx, $version);

      if(!$details) {
         $this->parent->showTextImage("The image (". $idx .") you requested is unknown");
         return;
      }

      /* no width specified - show photo in its original size */
      if($width == 0) {
         $fullpath = $this->parent->translate_path($this->parent->parse_uri($details['uri'], 'fullpath'));
      }
      /* show thumbnail */
      else {

         if(!$this->parent->is_valid_width($width)) {
            $this->parent->showTextImage("Requested width ". $width ."px is not valid!");
            return;
         }
         /* check for an entry if we already handled this photo before. If not,
            create a thumbnail for it.
         */
         if(!$this->parent->getMD5($idx)) {
            $this->parent->gen_thumb($idx);
         }
         /* get the full filesystem path to the thumbnail */
         $fullpath = $this->parent->get_thumb_path($width, $idx, $version);
         /* if the thumb file does not exist, create it */
         if(!file_exists($fullpath)) {
            $this->parent->gen_thumb($idx);
         }
      }

      if(!file_exists($fullpath)) {
         $this->parent->showTextImage("File ". basename($fullpath) ." does not exist");
         return;
      }
      if(!is_readable($fullpath)) {
         $this->parent->showTextImage("File ". basename($fullpath) ." is not readable. Check the permissions");
         return;
      } 
      $mime = $this->parent->get_mime_info($fullpath);

      if(!$this->parent->checkifImageSupported($mime)) {
         $this->parent->showTextImage("Unsupported Image Type");
         return;
      }

      Header("Content-Type: ". $mime);
      Header("Content-Length: ". filesize($fullpath));
      Header("Content-Transfer-Encoding: binary\n");
      Header("Content-Disposition: inline; filename=\"". $this->parent->parse_uri($details['uri'], 'filename') ."\"");
      Header("Content-Description: ". $this->parent->parse_uri($details['uri'], 'filename'));
      Header("Accept-Ranges: bytes");
      Header("Connection: close");
      Header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
      Header("Cache-Control: no-cache");
      Header("Pragma: no-cache");

      $file = fopen($fullpath, "rb");
      fpassthru($file);
      @fclose($file);

   } // showImg()

   /**
    * sends a random photo of the requested tag to the browser
    *
    * this function will send a random photo to the client.
    * It is selected out by the provided $tagidx. It also try's
    * to create on-the-fly missing thumbnails via PHPFSPOT
    * gen_thumbs function.
    * @param integer $idx
    */
   public function showTagImg($tagidx)
   {
      $idx = $this->parent->get_random_tag_photo($tagidx);
      $width = 30;

      $details = $this->parent->get_photo_details($idx);

      if(!$details) {
         $this->parent->showTextImage("The image (". $idx .") you requested is unknown");
         return;
      }

      /* if no entry for this photo is yet in the database, create thumb */
      if(!$this->parent->getMD5($idx)) {
         $this->parent->gen_thumb($idx);
      }

      $version = $this->parent->get_latest_version($idx);

      $fullpath = $this->parent->get_thumb_path($width, $idx, $version);
      /* if the thumb file does not exist, create it */
      if(!file_exists($fullpath)) {
         $this->parent->gen_thumb($idx);
      }

      if(!file_exists($fullpath)) {
         $this->parent->showTextImage("File ". basename($fullpath) ." does not exist");
         return;
      }
      if(!is_readable($fullpath)) {
         $this->parent->showTextImage("File ". basename($fullpath) ." is not readable. Check the permissions");
         return;
      }

      $mime = $this->parent->get_mime_info($fullpath);

      if(!$this->parent->checkifImageSupported($mime)) {
         $this->parent->showTextImage("Unsupported Image Type");
         return;
      }

      Header("Content-Type: ". $mime);
      Header("Content-Length: ". filesize($fullpath));
      Header("Content-Transfer-Encoding: binary\n");
      Header("Content-Disposition: inline; filename=\"". $this->parent->parse_uri($details['uri'], 'filename') ."\"");
      Header("Content-Description: ". $this->parent->parse_uri($details['uri'], 'filename'));
      Header("Accept-Ranges: bytes");
      Header("Connection: close");
      Header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
      Header("Cache-Control: no-cache");
      Header("Pragma: no-cache");

      $file = fopen($fullpath, "rb");
      fpassthru($file);
      @fclose($file);

   } // showTagImg()

} // PHPFSPOT_IMG()

if(isset($_GET['idx']) && (is_numeric($_GET['idx']) || $_GET['idx'] == 'rand')) {

   $img = new PHPFSPOT_IMG;

   if(isset($_GET['width']) && is_numeric($_GET['width'])) 
      $width = $_GET['width'];
   else
      $width = NULL;

   if(isset($_GET['version']) && is_numeric($_GET['version']))
      $version = $_GET['version'];
   else
      $version = NULL;

   $img->showImg($_GET['idx'], $width, $version);

   exit(0);
}

if(isset($_GET['tagidx']) && is_numeric($_GET['tagidx'])) {

   $img = new PHPFSPOT_IMG;
   $img->showTagImg($_GET['tagidx']);

   exit(0);

}

?>
