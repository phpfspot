/***************************************************************************
 *
 * phpfspot, presents your F-Spot photo collection in Web browsers.
 *
 * Copyright (c) by Andreas Unterkircher
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

/**
 * show photo
 *
 * this function will be called by client and fetches
 * the single-photo view via AJAX from the server.
 * Furthermore it will scrollup the browser to the top
 * position so the image become visibile immediatley.
 */
function showPhoto(id, scrollup)
{
   /* is phpfspot skeleton really displayed? */
   if(!document.getElementById('content'))
      return;

   var content = document.getElementById('content');

   /* blank the screen */
   if(scrollup != undefined) {
      content.innerHTML = "";
   }

   /* fetch single-photo view from server */
   HTML_AJAX.replace(content, encodeURI(web_path + '/rpc.php?action=showphoto&id=' + id));

   /* scroll the window up to the top */
   if(scrollup != undefined) {
      window.scrollTo(0,0);
   }

   /* delete some global vars */
   delete(origHeight); origHeight = undefined;
   delete(origWidth); origWidth = undefined;
   delete(photo_details_pos); photo_details_pos = undefined;

} // showPhoto()

/**
 * scroll browser to the last shown photo
 *
 * this function will be called when user returns from single-photo
 * to the photo index. It will scroll down the window (if possible)
 * to the position of the last shown photo.
 */
function moveToThumb(thumb_id)
{
   if(thumb_id == undefined)
      return;

   if(thumbimg = document.getElementById('thumbimg' + thumb_id)) {
      window.scrollTo(0, findPos(thumbimg,'top')-100);
   }

} // moveToThumb()

/**
 * return position of object
 *
 * this function returns the position of an object.
 * depending on the parameter 'direction' it will either
 * return the X or Y position.
 */
function findPos(obj, direction) {
   var cur = 0;
   if (obj.offsetParent) {
      do {
         if(direction == 'left')
            cur += obj.offsetLeft;
         if(direction == 'top')
            cur += obj.offsetTop;
      } while (obj = obj.offsetParent);
   }
   return [cur];

} // findPos()

/**
 * opens the credits page
 */
function showCredits()
{
   var credits = document.getElementById("content");
   credits.innerHTML = HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=showcredits'));

} // showCredits()

/**
 * tag-selection handling
 *
 * this function is getting called by client to either
 * - add
 ∗ - delete
 * - modify tag-match condition
 *
 * It will then fetch the result from the server via AJAX
 * and updates the tag-selection.
 */
function Tags(mode, id)
{
   var objTemp = new Object();

   if(mode == "add") {
      // add tag to users session
      objTemp['action'] = 'addtag';
      objTemp['id'] = id;
   }
   else if(mode == "del") {
      // del tag from users session
      objTemp['action'] = 'deltag';
      objTemp['id'] = id;
   }
   else if(mode == "condition") {
      setCheckedValue(id, id.value);
      objTemp['action'] = 'tagcondition';
      objTemp['mode'] = id.value;
   }

   var retr = HTML_AJAX.post(web_path + '/rpc.php', objTemp);
   if(retr == "ok") {
      refreshAvailableTags();
      refreshSelectedTags();
      refreshPhotoIndex();
   }
   else {
      window.alert("Server message: "+ retr);
   }

} // Tags()

/**
 * update available-tags tag-cloud
 *
 * this function queries an actual version of the tag-cloud
 * for the available (not-selected) tags from the server via
 * AJAX.
 */
function refreshAvailableTags()
{
   // update available tags
   var avail_tags = document.getElementById('available_tags');
   if(avail_tags != undefined) {
      avail_tags.innerHTML = "Loading...";
      avail_tags.innerHTML = HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=show_available_tags'));
   }

} // refreshAvailableTags()

/**
 * update selected-tags list
 *
 * this function queries an actual version of the tag-list
 * for the selected tags from the server via AJAX.
 */
function refreshSelectedTags()
{
   // update selected tags
   var selected_tags = document.getElementById("selected_tags");
   if(selected_tags != undefined) {
      selected_tags.innerHTML = "Loading...";
      selected_tags.innerHTML = HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=show_selected_tags'));
   }

} // refreshSelectedTags()

/**
 * show photo index
 *
 * this function will fetch the photo-index view from
 * the server via AJAX. It's also used to browse through
 * the photo-index pages
 */
function showPhotoIndex(begin_with, last_photo)
{
   var url = web_path + "/rpc.php?action=show_photo_index";
   if(begin_with != undefined)
      url = url + '&begin_with=' + begin_with;
   if(last_photo != undefined)
      url = url + '&last_photo=' + last_photo;

   HTML_AJAX.replace(document.getElementById("content"), encodeURI(url));

} // showPhotoIndex()

/**
 * update photo index
 *
 * this function will be called, to request a refresh of the
 * photo index. this, for example, can be caused, when changing
 * the tag-selection.
 */
function refreshPhotoIndex()
{
   /* only invoke showPhotoIndex(), if photo-index is really shown */
   if(document.getElementById("index") != undefined || startup == 1) {
      showPhotoIndex();
      startup = 0;
   }

} // refreshPhotoIndex()

/**
 * blur cursor focus
 *
 * this function removes the focus-rectangle which may appear
 * when click on a link. Not always beautiful.
 */
function click(object)
{
   if(object.blur)
      object.blur();

} // click()

/**
 * change current radio-button setting
 *
 * This function will check the radio-button with the given value.
 * If no radio-button is currently displayed, this function will do
 * nothing.
 * If the given value does not exist, the existing radio buttons will
 * be reseted.
 */
function setCheckedValue(condition, value) {

   var count = condition.length;
   if(count == undefined) {
      condition.checked = (condition.value == value.toString());
      return;
   }
   for(var i = 0; i < count; i++) {
      condition[i].checked = false;
      if(condition[i].value == value.toString()) {
         condition[i].checked = true;
      }
   }

} // setCheckedValue()

/**
 * Invoke a search
 *
 * This function will be invoked by starting
 * any kind of search (tag-name, photo-name, date, ...).
 */
function startSearch()
{
   if(document.getElementById('date_from').value != undefined) {
      date_from = document.getElementById('date_from').value;
   }
   if(document.getElementById('date_to').value != undefined) {
      date_to = document.getElementById('date_to').value;
   }
      
   var objTemp = new Object();
   objTemp['action'] = 'search';

   if(document.getElementsByName('searchfor_tag')[0] != undefined &&
      document.getElementsByName('searchfor_tag')[0].value != "") {
      objTemp['for_tag'] = document.getElementsByName('searchfor_tag')[0].value;
   }
   if(document.getElementsByName('searchfor_name')[0] != undefined &&
      document.getElementsByName('searchfor_name')[0].value != "") {
      objTemp['for_name'] = document.getElementsByName('searchfor_name')[0].value;
   }
   if(document.getElementsByName('consider_date')[0] != undefined &&
      document.getElementsByName('consider_date')[0].checked == true) {
      objTemp['date_from'] = date_from;
      objTemp['date_to'] = date_to;
   }
   if(document.getElementsByName('consider_rate')[0] != undefined &&
      document.getElementsByName('consider_rate')[0].checked == true) {
      objTemp['rate_from'] = rate_search['from'];
      objTemp['rate_to'] = rate_search['to'];
   }

   var retr = HTML_AJAX.post(web_path + '/rpc.php', objTemp);
   if(retr == "ok") {
      refreshAvailableTags();
      refreshSelectedTags();
      showPhotoIndex();
   }
   else {
      window.alert("Server message: "+ retr);
   }

} // startSearch()

/**
 * enable/disable date search
 *
 * this function will either enable or disable the
 * input fields for the date-search
 */
function datesearch()
{
   var mode = true;

   if(document.getElementsByName('consider_date')[0].checked == true) {
      mode = false;
   }
      
   document.getElementById('date_from').disabled = mode;
   document.getElementById('date_to').disabled = mode;
 
} // datesearch()

/**
 * set view mode
 *
 * called for photo-index export. will return the
 * selected mode via AJAX from the server.
 */
function setViewMode(srv_webpath, mode)
{
   if(srv_webpath != undefined)
      web_path = srv_webpath;
   else
      web_path = '';

   var exprt = document.getElementById('output');
   exprt.innerHTML = "Loading...";
   exprt.innerHTML = HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=get_export&mode=' + mode));

} // setViewMode()

/**
 * reset all search-fields
 */
function clearSearch()
{
   if(document.getElementsByName('searchfor_tag')[0] != undefined)
      document.getElementsByName('searchfor_tag')[0].value = '';
   if(document.getElementsByName('searchfor_name')[0] != undefined)
      document.getElementsByName('searchfor_name')[0].value = '';

   if(document.getElementsByName('consider_date')[0] != undefined &&
      document.getElementsByName('consider_date')[0].checked == true) {
      document.getElementsByName('consider_date')[0].checked = false;
      datesearch();
   }
   if(document.getElementsByName('consider_rate')[0] != undefined &&
      document.getElementsByName('consider_rate')[0].checked == true) {
      document.getElementsByName('consider_rate')[0].checked = false;
   }

} // clearSearch()

/**
 * if the client is planless, ask the server what to do
 * next.
 */
function AskServerWhatToDo()
{
   return HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=what_to_do'));

} // AskServerWhatToDo()

/**
 * init phpfspot
 *
 * this function will be called when the browser opens phpfspot
 * the first time. It will fetch the tag-lists and will then
 * switch to the right view, which the browser got told from
 * the server (maybe someone hit the refresh button...).
 *
 * as parameter the server can set the correct webpath.
 * espacialley when using user-friendly url's, the browser
 * does not know the correct URLs to address images, stylesheets,
 * ... then.
 */
function init_phpfspot(srv_webpath)
{
   if(srv_webpath != undefined)
      web_path = srv_webpath;
   else
      web_path = '';

   /* always load list of available tags */
   //this should not be more necessary since 4.5.08
   //refreshAvailableTags();

   /* ask the server what we are currently displaying */
   whattodo = AskServerWhatToDo();

   if(whattodo == 'showpi' || whattodo == 'showpi_date') {
      showPhotoIndex();
   }
   if(whattodo == 'showpi_tags') {
      refreshSelectedTags();
      showPhotoIndex();
   }
   if(whattodo == 'show_photo') {
      if(photo = getPhotoToShow()) {
         showPhoto(photo);
         refreshSelectedTags();
      }
   }

} // init_phpfspot()

/**
 * change background-color on mouse-over
 */
function setBackGrdColor(item, color)
{
   if(color == 'mouseover')
      item.style.backgroundColor='#c6e9ff';
   if(color == 'mouseout')
      item.style.backgroundColor='#eeeeee';
   if(color == 'mouseclick')
      item.style.backgroundColor='#93A8CA';

} // setBackGrdColor()

/**
 * ask server, which photo needs to be shown
 *
 * when user press the refresh-button in a single-photo
 * view or maybe enters the link via an external URL, the
 * client does not know, what photo will be shown (dimensions...).
 * But the server can tell this the browser.
 */
function getPhotoToShow()
{
   var photo_to_show = HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=get_photo_to_show'));

   // if no image needs to be shown, return false from here
   if(photo_to_show == "")
      return false;
   
   return photo_to_show;

} // getPhotoToShow()

/**
 * a fake-zoom for photo
 *
 * a quick to let the browser do some zooming on
 * photos.
 */
function zoom(mod)
{
   var photo;

   if(mod == undefined)
      return;

   /* internet explorer */
   if(document.images['photo'].width)
      photo = document.images['photo'];

   /* all others */
   if(photo == undefined && document.getElementById('photo').width)
      photo = document.getElementById('photo');

   if(photo != undefined) {

      if(origWidth == undefined)
         origWidth = photo.width;
      if(origHeight == undefined)
         origHeight = photo.height;

      if(mod != 0) {
         new_w = photo.width * (1 + mod/100);
         new_h = photo.height * (1 + mod/100);
         photo.width = new_w;
         photo.height = new_h;

         if(photo_details_pos == undefined) {
            photo_details_pos = findPos(document.getElementById('photo_details'),'left');
         }

         if((photo.offsetLeft + new_w) >= photo_details_pos-20) {
            hidePhotoDetails('true');
         }
         else {
            hidePhotoDetails('false');
         }
      }
      else {
         photo.width = origWidth;
         photo.height = origHeight;
         hidePhotoDetails('false');
      }
   }

} // zoom()

/**
 * hides the photo details layin
 *
 * if the photo is getting zoomed quiet large, this will
 * auto-hide (and also restore) the photo-details-box.
 */
function hidePhotoDetails(mode)
{
   var photo_details;

   if(photo_details = document.getElementById('photo_details')) {
      if(mode == 'true') {
         photo_details.style.visibility = 'hidden';
         photo_details.style.display = 'none';
      }
      else {
         photo_details.style.visibility = 'visible';
         photo_details.style.display = '';
      }
   }
} // hidePhotoDetails()

/**
 * show calendar
 */
function showCalendar(date_box, click_obj)
{
   var calendar;
   var userdate;

   calendar = document.getElementById('calendar');
   if(calendar == undefined) {
      window.alert("Can not find element 'calendar'");
      return;
   }

   userdate = document.getElementById('date_' + date_box);

   if(userdate == undefined) {
      window.alert("Can not find element 'date_'" + date_box);
      return;
   }

   userdate = userdate.value;

   if(date_box == 'from') {
      var xpos = document.getElementById('frompic').offsetLeft;
      var ypos = document.getElementById('frompic').offsetTop;
      calendar_mode = 'from';
   }
   if(date_box == 'to') {
      var xpos = document.getElementById('topic').offsetLeft;
      var ypos = document.getElementById('topic').offsetTop;
      calendar_mode = 'to';
   }
   calendar.style.left = xpos + 100 + 'px';
   calendar.style.top = ypos + 120 + 'px';

   if(calendar.style.visibility == "" || calendar.style.visibility == 'hidden') {
      calendar.style.visibility = 'visible';
      calendar.innerHTML = "Loading...";
      calendar.innerHTML = HTML_AJAX.grab(encodeURI(web_path +'/rpc.php?action=get_calendar_matrix&date=' + userdate));
      calendar_shown = 1;
   }
   else {
      hideCalendar();
   }

} // showCalendar()

/**
 * hide calendar
 */
function hideCalendar()
{
   var calendar = document.getElementById('calendar');
   if(calendar.style.visibility != 'hidden') {
      calendar.style.visibility = 'hidden';
      calendar_shown = 0;
   }
} // hideCalendar()

/**
 * switch month in calendar
 */
function setMonth(year, month, day)
{
   var calendar = document.getElementById('calendar');
   calendar.innerHTML = "Loading...";
   calendar.innerHTML = HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=get_calendar_matrix&date='+ year +'-'+ month +'-'+ day));
}

/**
 * get the user-selected date from the calendar and
 * put it into the date-search boxes
 */
function setCalendarDate(userdate)
{
   document.getElementById('date_'+calendar_mode).value = userdate;
   hideCalendar();

} // setCalendarDate()

/**
 * reset phpfspot complelely and move to the begining
 */
function resetAll()
{
   HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=reset'));
   clearSearch();
   refreshAvailableTags();
   refreshSelectedTags();
   refreshPhotoIndex();

} // resetAll()

/**
 * find objects with their class-name
 */
function WSR_getElementsByClassName(oElm, strTagName, oClassNames){
   var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
   var arrReturnElements = new Array();
   var arrRegExpClassNames = new Array();
   if(typeof oClassNames == "object"){
      for(var i=0; i<oClassNames.length; i++){
         arrRegExpClassNames.push(new RegExp("(^|\s)" + oClassNames[i].replace(/-/g, "\-") + "(\s|$)"));
      }
   }
   else{
      arrRegExpClassNames.push(new RegExp("(^|\s)" + oClassNames.replace(/-/g, "\-") + "(\s|$)"));
   }
   var oElement;
   var bMatchesAll;
   for(var j=0; j<arrElements.length; j++){
      oElement = arrElements[j];
      bMatchesAll = true;
      for(var k=0; k<arrRegExpClassNames.length; k++){
         if(!arrRegExpClassNames[k].test(oElement.className)){
            bMatchesAll = false;
            break;
         }
      }
      if(bMatchesAll){
         arrReturnElements.push(oElement);
      }
   }
   return (arrReturnElements)

} // WSR_getElementsByClassName()

/**
 * javascript based photo preloading
 */
function preloadPhotos(image_url) {

   var i;
   var timeout = 1000;
   var waiting = 100;
   var counting;

   lbImg = WSR_getElementsByClassName(document,"img","thumb");
   if(lbImg != undefined) {
      for(i=0;i<lbImg.length;i++){
         lbImg[i].src=image_url[i];
         // to not bomb the server with requests, give the page some time
         // to load the images one by one. if a image exceeds the timeout,
         // the next image will be loaded.
         if(lbImg[i].complete != undefined && lbImg[i].complete != true) {
            counting = 0;
            while(lbImg[i].complete != true && counting < timeout) {
               window.setTimeout("noop()", waiting);
               counting+=waiting;
            }
         }
      }
   }

} // preloadPhotos()

/* a function that does nothing */
function noop() {}

/**
 * start slideshow
 */
function startSlideShow(srv_webpath)
{
   if(srv_webpath != undefined)
      web_path = srv_webpath;
   else
      web_path = '';

   if(!sliding) {
      HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=reset_slideshow'));
      nextSlide();
      sliding = setInterval("nextSlide()", sliding_time*1000);
      document.getElementById('stop_ico').src = web_path + "/resources/32_stop.png";
   }
   else {
      clearInterval(sliding);
      sliding = 0;
      document.getElementById('stop_ico').src = web_path + "/resources/32_play.png";
   }

} // startSlideShow()

/**
 * switch to next slide
 */
function nextSlide()
{
   var next_img = HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=get_next_slideshow_img'));
   document.getElementById('slide_img').src = next_img;

} // nextSlide()

/**
 * switch to previous slide
 */
function prevSlide()
{
   var prev_img = HTML_AJAX.grab(encodeURI(web_path + '/rpc.php?action=get_prev_slideshow_img'));
   document.getElementById('slide_img').src = prev_img;

} // prevSlide()

/**
 * interrupt slide show
 */
function pauseSlideShow()
{
   if(!sliding_paused) {
      sliding_paused = 1;
      clearInterval(sliding);
      document.getElementById('pause_ico').src = web_path + "/resources/32_play.png";
   }
   else {
      sliding_paused = 0;
      sliding = setInterval("nextSlide()", sliding_time*1000);
      document.getElementById('pause_ico').src = web_path + "/resources/32_pause.png";
   }

} // pauseSlideShow()

/**
 * start auto-browse
 */
function startAutoBrowse()
{
   if(!autobrowse) {
      autoBrowse();
      autobrowse = setInterval("autoBrowse()", 5000);
   }
   else {
      clearInterval(autobrowse);
      autobrowse = 0;
      document.getElementById('autobrowse_ico').src = web_path + "/resources/16_play.png";
   }

} // startAutoBrowser()

/**
 * auto-browsing
 */
function autoBrowse()
{
   if(document.getElementById('next_link')) {
      var next_link = document.getElementById('next_link').href;
      window.location.href = next_link;
      document.getElementById('autobrowse_ico').src = web_path + "/resources/16_pause.png";
   }
   /* we have reached the last photo */
   else {
      if(ab_ico = document.getElementById('autobrowse_ico'))
         ab_ico.src = web_path + "/resources/16_play.png";
      clearInterval(autobrowse);
   }

} // autoBrowse()

/**
 * initiate slider to modify slide-switching-speed
 */
function initSlider()
{
   var sliderEl = document.getElementById ? document.getElementById("slider-1") : null;
   var inputEl = document.forms[0]["slider-input-1"];
   var s = new Slider(sliderEl, inputEl);
   s.setMinimum(1);
   s.setMaximum(10);
   s.setValue(sliding_time);
   document.getElementById("current_slide_time").innerHTML = sliding_time + "s Interval";
   s.onchange = function () {
      sliding_time = s.getValue();
      document.getElementById("current_slide_time").innerHTML = sliding_time + "s Interval";
      if(!sliding_paused && sliding) {
         clearInterval(sliding);
         sliding = setInterval("nextSlide()", sliding_time*1000);
      }
   };
   window.onresize = function () {
      s.recalculate();
   };

} // initSlider()

/**
 * if the sort-order (photo-name, date, ...) has been
 * changed, update the photo-index view.
 */
function update_sort_order(obj)
{  
   var objTemp = new Object();
   objTemp['value'] = obj.options[obj.selectedIndex].value;

   var retr = HTML_AJAX.post(web_path + '/rpc.php?action=update_sort_order', objTemp);

   if(retr == "ok") {
      showPhotoIndex();
   }
   else {
      window.alert("Server message: "+ retr);
   }

} // update_sort_order()

/**
 * if the photo-version ѕelect-box has changed, set the newly
 * choosen photo version as the to-be-displayed photo version
 */
function update_photo_version(obj, current_photo)
{
   var objTemp = new Object();
   objTemp['photo_version'] = obj.options[obj.selectedIndex].value;
   objTemp['photo_idx'] = current_photo;

   var retr = HTML_AJAX.post(web_path + '/rpc.php?action=update_photo_version', objTemp);

   if(retr == "ok") {
      showPhoto(current_photo);
   }
   else {
      window.alert("Server message: "+ retr);
   }

} // update_photo_version()

/**
 * show rate stars
 *
 * this function will show the requested amount of
 * rate-stars.
 *
 * @param string mode
 * @param int level
 */
function show_rate(mode, level)
{
   var i;

   for(i = 1; i <= 5; i++) {
      if(i <= level) {
         document.getElementById('rate_' + mode + '_' + i).src = web_path + '/resources/star.png';
      }
      else {
         document.getElementById('rate_' + mode + '_' + i).src = web_path + '/resources/empty_rate.png';
      }
   }

} // show_rate()

/**
 * set rate stars
 *
 *
 * this function will set the requested rate-stars-amount into a global
 * variable (which will then later be used on form-submit) and will also
 * update the display.
 *
 * @param string mode
 * @param int level
 */
function set_rate(mode, level)
{
   rate_search[mode] = level;
   show_rate(mode, level);

} // set_rate()

/**
 * reset rate stars
 *
 * this function will reset the rate-star to their initial value.
 *
 * @param string mode
 */
function reset_rate(mode)
{
   if(rate_search[mode] == undefined)
      rate_search[mode] = 0;

   show_rate(mode, rate_search[mode]);

} // reset_rate()

/**
 * handle key events
 */
function keyDown(e) {
   var evt = (e) ? e:(window.event) ? window.event:null;

   if(evt) {
      var key = (evt.charCode) ? evt.charCode :
         ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));


      if(key == 37) /* left curosr */ {
         if(document.getElementById('prev_link')) {
            var prev_link = document.getElementById('prev_link').href;
            window.location.href = prev_link;
         }
         return;
      }
      if(key == 38) /* up cursor */ {
      }
      if(key == 39) /* right curosr */ {
         if(document.getElementById('next_link')) {
            var next_link = document.getElementById('next_link').href;
            window.location.href = next_link;
         }
         return;
      }
      if(key == 73 && evt.altKey && evt.ctrlKey) /* ctrl+alt+i */ {
         showPhotoIndex();
         return;
      }
      if(key == 82 && evt.altKey && evt.ctrlKey) /* ctrl+alt+r */ {
         resetAll();
         return;
      }
   }
}

document.onkeydown=keyDown;
if(document.layers) {
   document.captureEvents(Event.KEYDOWN);
}

// will be reseted by first viewing photo-index
var startup = 1;
// calendar specific
var calendar_shown = 0;
var calendar_mode = '';
// auto-browsing & sliding
var autobrowse = 0;
var sliding = 0;
var sliding_paused = 0;
var sliding_time = 3;
// zooming
var origHeight;
var origWidth;
// position of the last shown photo in photo-index
var photo_details_pos;
var web_path;
var rate_search = new Array();
