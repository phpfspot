<?php

/***************************************************************************
 *
 * phpfspot, presents your F-Spot photo collection in Web browsers.
 *
 * Copyright (c) by Andreas Unterkircher
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

/**
 * PHPFSPOT_DB class
 *
 * @package phpfspot
 */
class PHPFSPOT_DB {

   private $db;
   private $db_path;
   private $parent;
   private $is_connected;
   private $last_error;
   private $last_query;

   /**
    * PHPFSPOT_DB class constructor
    *
    * This constructor initially connect to the database.
    */
   public function __construct($parent, $db_path)
   {
      global $phpfspot;

      $this->parent = $phpfspot;
      $this->db_path = $db_path;

      /* We are starting disconnected */
      $this->setConnStatus(false);

      /* Connect to database */
      $this->db_connect();

   } // __construct()
	 
   /**
    * PHPFSPOT_DB class deconstructor
    *
    * This destructor will close the current database connection.
    */ 
   public function __destruct()
   {
      $this->db_disconnect();

   } // _destruct()

   /**
    * PHPFSPOT_DB database connect
    *
    * This function will connect to the database
    */
   private function db_connect()
   {
      switch($this->parent->cfg->db_access) {
         case 'native':
            if(($this->db = sqlite3_open($this->db_path)) === false) {
               $this->throwError("Unable to connect to database:" . $this->getLastError());
               $this->setConnStatus(false);
            }
            else {
               sqlite3_create_function($this->db, 'basename', 1, 'basename');
               $this->setConnStatus(true);
            }
            break;
         case 'pdo':
            try {
               $this->db =  new PDO("sqlite:".$this->db_path);
               $this->setConnStatus(true);
            }
            catch (Exception $e) {
               $this->throwError("Unable to connect to database:" . $e->getMessage());
               $this->setConnStatus(false);
            }
            break;

      }

   } // db_connect()

   /**
    * PHPFSPOT_DB database disconnect
    *
    * This function will disconnected an established database connection.
    */
   private function db_disconnect()
   {
      switch($this->parent->cfg->db_access) {
         case 'native':
            if($this->getConnStatus())
               sqlite3_close($this->db);
            break;
         case 'pdo':
            $this->db = NULL;
            break;
      }

   } // db_disconnect()

   /**
    * PHPFSPOT_DB database query
    *
    * This function will execute a SQL query and return the result as
    * object.
    */
   public function db_query($query = "")
   {
      if(!$this->getConnStatus())
         return false;
   
      $this->last_query = $query;

      switch($this->parent->cfg->db_access) {
         case 'native':
            if(($result = sqlite3_query($this->db, $query)) === false)
               $this->ThrowError($this->getLastError());
            break;
         case 'pdo':
            try{
               $result = $this->db->query($query);
               return $result;
            }
            catch (Exception $e) {
               $this->ThrowError($e->getMessage());
               $result= NULL;
            }
            break;

      }

      return $result;

   } // db_query()

   /**
    * PHPFSPOT_DB database query & execute
    *
    * This function will execute a SQL query and return nothing.
    */
   public function db_exec($query = "")
   {
      if(!$this->getConnStatus())
         return false;

      $this->last_query = $query;

      switch($this->parent->cfg->db_access) {
         case 'native':
            if(($result = sqlite3_exec($this->db, $query)) === false) {
               $this->ThrowError($this->getLastError());
               return false;
            }
            break;
         case 'pdo':
            try {
               $result = $this->db->query($query);
            }
            catch (Exception $e){
               $this->ThrowError($e->getLMessage());
               return false;
            }
            break;
      }

      return true;

   } // db_exec()

   public function db_fetch_object($resource)
   {
      switch($this->parent->cfg->db_access) {
         case 'native':
            return sqlite3_fetch_array($resource);
         case 'pdo':
            return $resource->fetch();
      }

   } // db_fetch_object

   /**
    * PHPFSPOT_DB fetch ONE row
    *
    * This function will execute the given but only return the
    * first result.
    */
   public function db_fetchSingleRow($query = "") 
   {
      if(!$this->getConnStatus())
         return false;

      $result = $this->db_query($query);
      switch($this->parent->cfg->db_access) {
         case 'native':
            $row = $this->db_fetch_object($result);
            break;
         case 'pdo':
            $row = $result->fetch();
            break;
      }
      return $row;
      
   } // db_fetchSingleRow()

   /**
    * PHPFSPOT_DB number of affected rows
    *
    * This functions returns the number of affected rows but the
    * given SQL query.
    */
   public function db_getNumRows($query = "")
   {
      /* Execute query */
      $result = $this->db_query($query);

      /* Errors? */
      if(PEAR::isError($result)) 
         $this->throwError($result->getMessage());

      return $result->numRows();

   } // db_getNumRows()

   /**
    * PHPFSPOT_DB check table exists
    *
    * This function checks if the given table exists in the
    * database
    * @param string, table name
    * @return true if table found otherwise false
    */
   public function db_check_table_exists($table_name = "")
   {
      if(!$this->getConnStatus())
         return false;

      $result = $this->db_query("SELECT name FROM sqlite_master WHERE type='table'");
      switch($this->parent->cfg->db_access) {
         case 'native':
            while($table = $this->db_fetch_object($result)) {
               if($table['name'] == $table_name)
                  return true;
            }
            break;
         case 'pdo':
            foreach($result as $table ){
               if($table['NAME'] == $table_name)
                  return true;
            }
            break;
       }

      return false;
	 
   } // db_check_table_exists()

   /**
    * PHPFSPOT_DB check column exist
    *
    * This function checks if the given column exists within
    * the specified table.
    */
   public function db_check_column_exists($table_name, $column)
   {
      if(!$this->getConnStatus())
         return false;

      $result = $this->db_query("
         SELECT sql
         FROM
            (SELECT * FROM sqlite_master UNION ALL
             SELECT * FROM sqlite_temp_master)
         WHERE
            tbl_name LIKE '". $table_name ."'
         AND type!='meta'
         AND sql NOT NULL
         AND name NOT LIKE 'sqlite_%'
         ORDER BY substr(type,2,1), name
      ");

      while($row = $this->db_fetch_object($result)) {
         /* CREATE TABLE xx ( col1 int, col2 bool, col3 ...) */
         if(strstr($row['sql'], " ". $column ." ") !== false)
               return true;
      }

      return false;

   } // db_check_column_exists()

   /**
    * PHPFSPOT_DB get connection status
    *
    * This function checks the internal state variable if already
    * connected to database.
    */
   private function setConnStatus($status)
   {
      $this->is_connected = $status;
      
   } // setConnStatus()

   /**
    * PHPFSPOT_DB set connection status
    * This function sets the internal state variable to indicate
    * current database connection status.
    */
   private function getConnStatus()
   {
      return $this->is_connected;

   } // getConnStatus()

   /**
    * PHPFSPOT_DB throw error
    *
    * This function shows up error messages and afterwards through exceptions.
    */
   private function ThrowError($string)
   {
      if(!defined('DB_NOERROR'))  {
         print "Error during query: ". $this->last_query ."<br /><br />\n";
         print "<br /><br />". $string ."<br /><br />\n";
         try {
            throw new Exception;
         }
         catch(Exectpion $e) {
         }
      }

      $this->last_error = $string;
	 
   } // ThrowError()

   private function getLastError()
   {
      switch($this->parent->cfg->db_access) {
         case 'native':
            return sqlite3_error($this->db);
         case 'pdo':
            return "". $this->db->errorInfo();
      }

   } // getLastError()

   /**
    * start transaction
    *
    * this will start a transaction on ACID-supporting database
    * systems.
    *
    * @return bool
    */
   public function db_start_transaction()
   {
      if(!$this->getConnStatus())
         return false;

      $result = $this->db_exec("BEGIN");

      /* Errors? */
      if(PEAR::isError($result))
         $this->throwError($result->getMessage() .' - '. $result->getUserInfo());

      return true;

   } // db_start_transaction()

   /**
    * commit transaction
    *
    * this will commit an ongoing transaction on ACID-supporting
    * database systems
    *
    * @return bool
    */
   public function db_commit_transaction()
   {
      if(!$this->getConnStatus())
         return false;

      $result = $this->db_exec("COMMIT");

      /* Errors? */
      if(PEAR::isError($result))
         $this->throwError($result->getMessage() .' - '. $result->getUserInfo());

      return true;

   } // db_commit_transaction()

   /**
    * rollback transaction()
    *
    * this function aborts a on going transaction
    *
    * @return bool
    */
   public function db_rollback_transaction()
   {
      if(!$this->getConnStatus())
         return false;

      $result = $this->db_exec("ROLLBACK");

      /* Errors? */
      if(PEAR::isError($result))
         $this->throwError($result->getMessage() .' - '. $result->getUserInfo());

      return true;

   } // db_rollback_transaction()

} // PHPFSPOT_DB

?>
