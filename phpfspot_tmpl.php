<?php

/***************************************************************************
 *
 * phpfspot, presents your F-Spot photo collection in Web browsers.
 *
 * Copyright (c) by Andreas Unterkircher
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

/**
 * PHPFSPOT_TMPL class
 *
 * Extends Smarty base class with some additional functions
 * @package phpfspot
 */
class PHPFSPOT_TMPL extends Smarty {

   /**
    * PHPFSPOT_TMPL constructor
    * @access public
    */
   public function __construct()
   {
      global $phpfspot;

      if(!file_exists($phpfspot->cfg->base_path .'/themes/'. $phpfspot->cfg->theme_name .'/templates')) {
         print "No templates found in ". $phpfspot->cfg->base_path .'/themes/'. $phpfspot->cfg->theme_name .'/templates';
         exit(1);
      }

      $this->Smarty();
      $this->template_dir = $phpfspot->cfg->base_path .'/themes/'. $phpfspot->cfg->theme_name .'/templates';
      $this->compile_dir  = $phpfspot->cfg->base_path .'/templates_c';
      $this->config_dir   = $phpfspot->cfg->base_path .'/smarty_config';
      $this->cache_dir    = $phpfspot->cfg->base_path .'/smarty_cache';

      if(isset($phpfspot->cfg->use_lightbox) && $phpfspot->cfg->use_lightbox == true)
         $this->assign('use_lightbox', 'true');
      if(isset($phpfspot->cfg->use_autocomplete) && $phpfspot->cfg->use_autocomplete == true)
         $this->assign('use_autocomplete', 'true');

   } // __construct()

   /**
    * show template
    *
    * outputs the requested template
    * @param string $template
    */
   public function show($template)
   {
      $this->display($template);

   } // show()

} // PHPFSPOT_TMPL()

?>
