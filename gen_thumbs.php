#!/usr/bin/php
<?php

/***************************************************************************
 *
 * phpfspot, presents your F-Spot photo collection in Web browsers.
 *
 * Copyright (c) by Andreas Unterkircher
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/


/**
 * gen_thumbs.php
 *
 * generate photo thumbnails on the console to avoid webserver overload
 *
 * @package phpfspot
 */
if(!isset($_SERVER["TERM"])) {
   print "<br /><br />This script should only be invoked from command line!<br />\n";
   die;
}

require_once "phpfspot.class.php";

$fspot = new PHPFSPOT;
$fspot->fromcmd = true;

$overwrite = false;

$short_options = "";
$short_options.= "h"; /* help */
$short_options.= "o"; /* overwrite */
$short_options.= "c"; /* cleanup */

$long_options = array(
   "help",
   "overwrite",
   "cleanup",
);

/* command line option specified? */
if(isset($_SERVER['argc']) && $_SERVER['argc'] > 1) {
   /* validate */
   $con = new Console_Getopt;
   $args = $con->readPHPArgv(); 
   $options = $con->getopt($args, $short_options, $long_options);

   if(PEAR::isError($options)) {
      die ("Error in command line: " . $options->getMessage() . "\n");
   }

   foreach($options[0] as $opt) {
      switch($opt[0]) {
         case 'h':
         case '--help':
            print "we need some help here!\n";
            exit(0);
            break;
         case 'o':
         case '--overwrite':
            print "Overwrite flag set!\n";
            $overwrite = true;
            break;
         case 'c':
         case '--cleanup':
            $fspot->cleanup_phpfspot_db();
            exit(0);
            break;
         default:
            print "invalid option";
            exit(1);
            break;
      }
   }
}

$all = $fspot->getPhotoSelection();

foreach($all as $photo) {
   $fspot->gen_thumb($photo, $overwrite);
}

?>
