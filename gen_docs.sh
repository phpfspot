#!/bin/bash

# create phpDocumentor docs from phpfspot source code

phpdoc -o HTML:frames:earthli \
       -d . \
       -t docs \
       -i 'thumbs/,resources/,templates_c/,phpfspot_cfg.php' \
       -ti 'phpfspot source-code documentation' \
       -dn 'phpfspot' \
       -s \
       -q
